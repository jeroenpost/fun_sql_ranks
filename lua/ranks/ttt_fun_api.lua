if (not ConVarExists("server_id")) then
    CreateConVar("server_id", "9999", { FCVAR_ARCHIVE }, "The ID of the Server")
end
if (not ConVarExists("server_rank")) then
    CreateConVar("server_rank", "9999", { FCVAR_ARCHIVE }, "The rank of the Server")
end
fun_api = {}
local Player = FindMetaTable('Player')
server_id = GetConVarNumber("server_id");

timer.Simple(1,function()
    function Player:PS_SendPoints(wat)
        self:PS_Save()

        local points = self.PS_Points
        if self.PS_Points >  2000000000 then
            points = 2000000000
        end
        if self.PS_Points < 1 then
            points = 0
            self.PS_Points = 0
        end
        net.Start('PS_Points')
        net.WriteEntity(self)
        net.WriteInt(points, 32)
        net.Broadcast()
    end
end)

fun_api.OfflineRetry = function(ply)
    timer.Simple(30, function()
        if IsValid(ply) then
            ply:ChatPrint("The Database is offline. Ill try to get your points again in 30 seconds")
            ply:ChatPrint("Don't worry, once the database is back, your points and time will be back")
            fun_api.LoadUserData(ply)
        end
    end)
end

fun_api.resendPoints = function( ply )
    ply:PS_SendPoints()
    ply:PS_SendItems()
    timer.Simple(6, function()
        if IsValid(ply) then
            ply:PS_SendPoints()
            ply:PS_SendItems()
        end
    end)
end


fun_api.LoadUserData = function(ply)
    if (not IsValid(ply) or ply.hasHisData ) then return end

    ply.adminStatus = false
    ply.PS_Points = 0
    ply.PS_Items = {}
    ply.PS_Equipped = {}
    ply.Premium = FALSE


    if ply:SteamID() == "BOT" then return end

    if not gb_json.online then
        fun_api.OfflineRetry()
        return
    end
    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                ['action']="getuserinfo",['steam']= ply:SteamID() },
        function( body, len, headers, code )
            if not IsValid(ply) then return end
            if body == "NOTFOUND" then
                fun_api.createNewUser(ply)
                return
            end
            local userdata = util.JSONToTable( body )

            if not userdata or not userdata.utime or tonumber(userdata.utime) < 60 then

                ply:ChatPrint("### There were problems getting your data. Trying again in 10 seconds ###")
                print("### ERROR ### Problems loading data for "..ply:Nick())
                timer.Simple(15,function() if IsValid(ply) then fun_api.LoadUserData(ply) end end)
                return
            end

            -- Check the bans and exit if person is banned
            if not fun_api.check_ban( userdata, ply) then
                return
            end
            -- Get the admin status

            if not fun_api.check_hidden_admin( userdata, ply ) then
                 fun_api.check_adminstatus( userdata, ply )
                 fun_api.check_and_set_usergroup( userdata, ply )
            end

            ply:SetUTime( tonumber(userdata.utime) )
            ply:SetUTimeStart(CurTime())
            ply.PS_Points = PS:ValidatePoints(tonumber(userdata.ps_points))
            ply.PS_Items =  util.JSONToTable(userdata.ps_items or '{}')
            if type(ply.PS_Items) ~= 'table' then ply.PS_Items = {} end

             ply.isfunadmin = nil
             ply.isjrmod = nil
             ply.ismod = nil
             ply.isdonator = nil
             ply.isdonatorplus = nil
             ply.issuperdonator = nil
             ply.ismegadonator = nil

             timer.Create("resetUserTanksranks",5,3,function()
             if(!IsValid(ply)) then return end
                             ply.isfunadmin = nil
                             ply.isjrmod = nil
                             ply.ismod = nil
                             ply.isdonator = nil
                             ply.isdonatorplus = nil
                             ply.issuperdonator = nil
                             ply.ismegadonator = nil

             end)

            -- Group rewards
            ply.isfungroupmember = false
            ply.is_website_member = false
            ply.got_group_reward = false
            ply.got_website_reward = false
            if userdata.is_member and tonumber(userdata.is_member) == 1 then
                ply.isfungroupmember = true
            end
            if userdata.is_website_member and  tonumber(userdata.is_website_member) == 1 then
                ply.is_website_member = true
            end
            if userdata.got_group_reward and  tonumber(userdata.got_group_reward) == 1 then
                ply.got_group_reward = true
            end
            if userdata.got_website_reward and tonumber(userdata.got_website_reward) == 1 then
                ply.got_website_reward = true
            end


            fun_api.resendPoints( ply )
            ply.hasHisData = true

            -- Nag about bought weapons
            if  GetConVarString("gamemode") == "terrortown" and (server_id == 53 or server_id == 100) and not without_customs then
                fun_api.get_person_weapons( ply, false )
            end

            -- Nag about donations
            fun_api.donationstatus_to_chat(userdata, ply)

            -- Nag about unredeemed points
            fun_api.get_unredeemed_points(userdata, ply)

            if GetConVarString("gamemode") == "laststand"  then
                if gb.is_superdonator(ply) then
                    ply.Premium = true
                end
            end
        end,
        function()
            fun_api.OfflineRetry()
        end)
end


fun_api.createNewUser = function(ply)
    ply:SetUTime(60)
    ply:SetUTimeStart(CurTime())
    ply.PS_Points = 2500
    ply.PS_Items = {}
    fun_api.resendPoints( ply )
    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action'] = "create_user", ['steam'] = ply:SteamID() },
            function(data)
                if(data == "EXISTS") then
                    ply.hasHisData = false
                    fun_api.LoadUserData(ply)
                    ply:ChatPrint("Reloading your data....")
                    return
                end
                fun_api.save_user_data( ply )
                ply:ChatPrint("Hi! Welcome to the TTT-FUN servers! We just created a new account for you!")
            end);

end

fun_api.get_all_bans = function()
    print("### Reloading Bans ###")
    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="get_all_bans" },
        function( body, len, headers, code )
            local bans = util.JSONToTable( body )
            fun_api.all_bans = bans

        end)
    -- Get them again after 5 minutes
    timer.Create("ttt_fun_api_reload_bans",300,10,function() fun_api.get_all_bans() end)
end

fun_api.check_ban = function( row, ply )

    local steam = ply:SteamID()
    if fun_api.all_bans and istable(fun_api.all_bans) then
        for _,w in ipairs( fun_api.all_bans ) do
            if( w.steam == steam ) then
                ply:Kick("You are banned from this server. Reason: "..w.why..". Your ban ends in: "..w.timeend.." You can appeal your ban at ttt-fun.com");
                return false
             end
        end

        if (row and row.banned_till ~= nil and tonumber(row.banned_till) > os.time()) then
            ply:Kick("You are banned from this server till "..os.date("%c", row.banned_till )..". You can appeal your ban at https://ttt-fun.com/ ");
            return false
        end
    end

    return true
end

fun_api.check_adminstatus = function(row, ply)
    if row.groups == nil or row.groups == "" or not row.groups then
        row.groups = "user"
    end
    ply.baseGroup = row.groups


    if row.admin_group then
        ply.adminStatus = tostring(row.admin_group)
    else
        ply.adminStatus = false
    end
end

fun_api.check_hidden_admin = function( row, ply )
    if row.hidden_admin != "1" then
        return false
    end
    fun_api.set_usergroup("hidden_staff", ply )
    local message = "You are hidden as staff. Don't use this longer than nessesary. Type !normal_staff to switch back."
    ply:PrintMessage( HUD_PRINTTALK, message )

    timer.Create("fun_api_nag_user_about_hidden_admin"..ply:SteamID(),20,0,function()
        if not IsValid(ply) then return end
        ply:PS_Notify( message )
        ply:PrintMessage( HUD_PRINTTALK, message )
    end)
    return true
end

fun_api.set_usergroup = function( group, ply)
    local id =  ply:SteamID():upper() -- Steam id needs to be upper
    if not ULib.ucl.groups[ group ] then return end
    ULib.ucl.addUser( id, allows, denies, group )
    print("###Group set### "..group.." ### " .. ply:Nick())
    ply:SetUserGroup( group )
    timer.Simple(15,function() if not IsValid(ply) then return end ply:SetUserGroup(group) end )
    timer.Simple(15,function() if not IsValid(ply) then return end
        hook.Call( ULib.HOOK_UCLCHANGED )
        hook.Call( ULib.HOOK_UCLAUTH, _, ply )
    end)
end

fun_api.check_and_set_usergroup = function(row, ply)
    if not ply.adminStatus or ply.adminStatus == false or ply.adminStatus == "" then
        if ply:GetUserGroup() ~= row.groups then
            if (row.groups == "user") then
                RunConsoleCommand("ulx", "removeuserid", ply:SteamID())
            else
                fun_api.set_usergroup( row.groups ,ply )
            end
        end
    else
        if ply:GetUserGroup() ~= ply.adminStatus and ply.adminStatus and ply.adminStatus != "" then
            fun_api.set_usergroup( ply.adminStatus ,ply)
        end
    end

    -- Throw them in the ranks table
    if not ply.ranks then
        ply.ranks = {}
    end
    if row.admin_group and row.admin_group ~= "" then
        table.insert(ply.ranks, row.admin_group)
    end
    if row.groups and row.groups ~= "" then
        table.insert(ply.ranks, row.groups)
    end

    // Set custom rank thingy
    if ATAG then
      local var = ATAG.CH_SendCanSetOwnTag(ply)
    end

    -- Check it out in 15 seconds
    timer.Simple(15,function()
        if not IsValid(ply) then return end
        local currentrank = ply:GetUserGroup()

        if currentrank == "user" or currentrank == "" or currentrank == "member" then
            for k,v in pairs(ply.ranks) do
                if v == "megadonator" then
                    fun_api.set_usergroup("megadonator",ply)
                    return
                end
                if v == "super_donator" and ply:GetUserGroup() != "megadonator" then
                    fun_api.set_usergroup("super_donator",ply)
                end
                if v == "donatorplus" and ply:GetUserGroup() != "megadonator" and ply:GetUserGroup() != "super_donator" then
                    fun_api.set_usergroup("donatorplus",ply)
                end
                if v == "donator" and ply:GetUserGroup() != "megadonator" and ply:GetUserGroup() != "super_donator"  and ply:GetUserGroup() != "donatorplus" then
                    fun_api.set_usergroup("donator",ply)
                end
            end


            end
    end)
end

fun_api.get_person_weapons = function( calling_ply, printit )
    calling_ply.lastredeemWeps = CurTime()

    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="getuserweapons",['steam']= calling_ply:SteamID() },
        function( body, len, headers, code )

        calling_ply.lastredeemWeps = CurTime()
        calling_ply.weapons =  util.JSONToTable( body )

            if printit then
                calling_ply:PrintMessage( HUD_PRINTTALK,"================\nYOUR WEAPONS:\n================")
                for _,w in ipairs( calling_ply.weapons ) do
                    calling_ply:PrintMessage( HUD_PRINTTALK,w)
                end
                calling_ply:PrintMessage( HUD_PRINTTALK,"================")
            end

            if table.Count(calling_ply.weapons) > 0 then
                local message = "You have a bunch of weapons. Type: !showweapons to show your weapons. Type !getweapon <weaponname> to get one"
                calling_ply:PrintMessage( HUD_PRINTTALK, message )
                calling_ply:PS_Notify( message )
                timer.Create("ttt_fun_api_nag_weapons"..calling_ply:SteamID(),180,0,function()
                    if not IsValid(calling_ply) then return end
                    calling_ply:PS_Notify( message )
                    calling_ply:PrintMessage( HUD_PRINTTALK, message )
                end)
            end

        end)
end

fun_api.donationstatus_to_chat = function( row, ply )
    if not row.donator_end_time then row.donator_end_time = 0 end
    if not row.total_donated then row.total_donated = 0 end
        if ply:GetUserGroup() == "donator" and row.donator_end_time ~= 0 and tonumber(row.total_donated) < 15 then

            local number = math.floor((tonumber(row.donator_end_time) - os.time()) / 86400)
            local text = ""
            if number < 2 then
                number = math.floor((tonumber(row.donator_end_time) - os.time()) / 3600)
                text = "Thanks for donating! Your donator status is valid for " .. number .. " more hours"
            else
                text = "Thanks for donating! Your donator status is valid for " .. number .. " more days"
            end

            ply:ChatPrint(text)

            timer.Create("ttt_fun_api_nag_donator" .. ply:SteamID(), 220, 30, function()
                if IsValid(ply) then
                    ply:ChatPrint(text)
                end
            end)
        end
end

fun_api.get_unredeemed_points = function( row, ply)
    --Set extra points if they should be set
    if row.extra_points == "" then row.extra_points = 0 end
    if (row.extra_points and tonumber(row.extra_points) > 0) then
        local message = "You have "..row.extra_points.." points in your bank. Say !redeem to redeem your points!"
        ply:PrintMessage( HUD_PRINTTALK, message )

        timer.Create("ttt_fun_api_nag_points"..ply:SteamID(),70,0,function()
            if not IsValid(ply) then return end
            ply:PS_Notify( message )
            ply:PrintMessage( HUD_PRINTTALK, message )
        end)
    end

    -- Get points for selling weapons
    if row.extrapointsupdate == "" then row.extrapointsupdate = 0 end
    if (row.extrapointsupdate and tonumber(row.extrapointsupdate) > 0) then

        local message = "You have "..row.extrapointsupdate.." points in your bank for selling weapons! Say !redeem_weaponpoints to redeem your points!"
        ply:PrintMessage( HUD_PRINTTALK, message )

        timer.Create("ttt_fun_api_nag_weaponpoints"..ply:SteamID(),120,0,function()
            if not IsValid(ply) then return end
            ply:PS_Notify( message )
            ply:PrintMessage( HUD_PRINTTALK, message )
        end)
    end
end

fun_api.set_hidden_staff = function( ply, hidden)
    if gb.is_mod( ply ) then
        if hidden == true then
            http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                ['action']="sethiddenstaff",['steam']= ply:SteamID() });
        else
            http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                ['action']="setnormalstaff",['steam']= ply:SteamID() });
        end
    end
end

fun_api.register_weapon_buy = function( singleuse, steam, owner, points, ps_id, name)
    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "register_weaponbuy",
        ['steam']   = steam,
        ['singleuse'] = tostring(singleuse),
        ['owner']   = owner,
        ['points']  = tostring(points),
        ['ps_id']   = tostring(ps_id),
        ['name']   = tostring(name)
    }
   http.Post( gb_json.settings.url, sendTable);
end

fun_api.save_all_user_data = function()
    for k,v in pairs( player.GetHumans()) do
        fun_api.save_user_data( v )
    end
end


fun_api.save_user_data = function(ply, doit)
    if not ply.last_save then ply.last_save = 0 end
    if (not IsValid(ply) or ply:IsBot() or not gb_json.online  ) then return end
    -- or (ply.last_save + 5 > CurTime() and not doit)

    ply.last_save = CurTime()
    local points = math.floor(ply.PS_Points or 5000)
    local utime = math.floor(ply:GetUTimeTotalTime())
    local nick = ply:Nick()
    if (utime < 120 or points < 150) then return end
    if ply.SteamName and isfunction(ply.SteamName) then
        nick = (ply:SteamName())
    end



    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "save_user_data",
        ['steam']   = ply:SteamID(),
        ['nick']    = nick,
        ['webnick'] = gb_json.base64enc(nick),
        ['points']  = tostring(math.floor(ply.PS_Points or 5000)),
        ['items']   = tostring(util.TableToJSON((ply.PS_Items) or {})),
        ['utime']   = tostring(utime),
        ['ip']      = string.sub(ply:IPAddress(), 1, (string.find(ply:IPAddress(), "%:") - 1)),
        ['server']  = tostring(server_id)
    }

    http.Post( gb_json.settings.url, sendTable ,function(data) print("Saved data for "..nick.." "..data.." utime:"..tostring(utime)) end);
end

fun_api.report_serverstats = function()

    local plys = {}

    for k,v in pairs( player:GetHumans() ) do

        if v.SteamName and isfunction(v.SteamName) then
            plys[ v:SteamID() ] = v:SteamName()
        else
            plys[ v:SteamID() ] = v:Nick()
        end
    end

    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "report_serverstats",
        ['port']      = tostring(GetConVarString("hostport")),
        ['ip']      = tostring(GetConVarString("ip")),
        ['server']  = tostring(server_id),
        ['servertype']  = tostring(server_type) or "unknown",
        ['players'] = tostring( table.Count(player:GetHumans())),
        ['maxplayers'] = tostring( game.MaxPlayers()),
        ['map'] = tostring( game.GetMap() ),
        ['hostname'] = tostring( GetConVarString("hostname")),
        ['gamemode'] = tostring(GetConVarString("gamemode") ),
        ['playerlist'] = tostring(util.TableToJSON(plys or {}))
    }

    http.Post( gb_json.settings.url, sendTable ,function(data) print("Posted serverstats "..data) end);
end

fun_api.get_server_rank = function()

    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "get_server_rank",
        ['server'] = tostring(server_id),
    }
    http.Post( gb_json.settings.url, sendTable ,function(data) print("### Server Rank "..data)
        RunConsoleCommand("server_rank", tonumber(data));
    end);

end

fun_api.player_gotgroupreward = function(ply)

    local group_reward = 0
    if ply.got_group_reward then group_reward = "1" end
    local member = 0
    if ply.isfungroupmember then member = "1" end
    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "player_gotgroupreward",
        ['steam']      = tostring(ply:SteamID()),
        ['is_member']  = tostring(member),
        ['is_website_member'] = tostring( ply.is_website_member),
        ['got_group_reward'] = tostring(  group_reward),
        ['got_website_reward'] = tostring(ply.got_website_reward )
    }

    http.Post( gb_json.settings.url, sendTable ,function(data) print("Posted player member stats "..data) end);
end

fun_api.safeStr = function( str )

    local pattern = "[^0-9a-zA-Z%s]+"; -- regex pattern

    local clean = tostring( str );

    if not clean or not str then return "" end

    local first, last = string.find( str, pattern );

    if( first != nil and last != nil ) then

    clean = string.gsub( clean, pattern, "" ); -- remove bad sequences

    end

    return clean;

end


if SERVER then
    hook.Add("PlayerInitialSpawn", "ttt_fun_api_load_user_data", fun_api.LoadUserData)
    hook.Add("PlayerDisconnected", "ttt_fun_api_safe_user_data", fun_api.save_user_data)
    hook.Add("Initialize", "ttt_fun_api_get_vars", timer.Simple(0.5,function()
        fun_api.get_all_bans()
        fun_api.get_server_rank()
    end))

   -- hook.Add( "ShutDown", "ttt_fun_api_shutdown", fun_api.save_all_user_data )
    hook.Add("TTTEndRound", "ttt_fun_api_all_user_datasave2", fun_api.save_all_user_data)



   if GetConVarString("gamemode") == "prop_hunt" then

       hook.Add("RoundEnd", "ttt_fun_api_all_user_datasave_ph", fun_api.save_all_user_data)
   end
  -- Get the rank

    --timer.Simple(5,function()  fun_api.report_serverstats() end)
    timer.Create("ttt_fun_api_report_serverstats",60,0,function() fun_api.report_serverstats() end)
end
