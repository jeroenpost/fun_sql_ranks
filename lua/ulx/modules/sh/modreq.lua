CATEGORY_NAME = "Mod Request"

------------------------------ addban ------------------------------
function ulx.addban( calling_ply, target_ply, reason )
	if target_ply:IsBot() then
		ULib.tsayError( calling_ply, "Cannot addban a bot", true )
		return
	end
	
	local bantime = "Sixhunderthirtytreebillion"
	local time = "for Sixhunderthirtytreebillion minute(s) from the Mod Request system"

	local str = "#A banned #T " .. time
	if reason and reason ~= "" then str = str .. " (#s)" end
	
	if SERVER then
		local banned = target_ply.ModReqBanned
		if(not banned) then
			ulx.fancyLogAdmin( calling_ply, str, target_ply, 0, reason )
			target_ply.ModReqBanned = true
		end
		ModReq.AddBan( calling_ply, target_ply, bantime, reason )
	end
end
local addban = ulx.command( CATEGORY_NAME, "ulx addban", ulx.addban, "!addban" )
addban:addParam{ type=ULib.cmds.PlayerArg }
addban:addParam{ type=ULib.cmds.StringArg, hint="reason", ULib.cmds.optional, ULib.cmds.takeRestOfLine, completes=ulx.common_kick_reasons }
addban:defaultAccess( ULib.ACCESS_ADMIN )
addban:help( "Bans target from the Mod Request System." )