
local CATEGORY_NAME = "Website"

------------------------------ printpoints ------------------------------
function ulx.printpoints( calling_ply  )


    local plys = player.GetAll()
    for i=1, #plys do
        local message = plys[ i ]:Nick()..": "..plys[ i ].PS_Points
        print( message);
        if IsValid(calling_ply) then
            calling_ply:PrintMessage( HUD_PRINTCONSOLE , message )
        end
    end
end
local printpoints = ulx.command( "TTT Fun", "ulx printpoints", ulx.printpoints )
printpoints:defaultAccess( ULib.ACCESS_ALL )
printpoints:help( "Print out the number of points per user in console" )

------------------------------ pspoint ------------------------------
function ulx.pspoint( calling_ply, target_plys, amount )

    for i=1, #target_plys do
        target_plys[i]:PS_GivePoints(amount)
        target_plys[i]:PS_Notify( "An admin gave you "..amount.." points" )
        --target_plys[ i ]:AddCredits(amount)
    end

    ulx.fancyLogAdmin( calling_ply, true, "#A has given #T #i Points", target_plys, amount )
end
local acred = ulx.command("TTT Fun", "ulx pspoints", ulx.pspoint, "!pspoints")
acred:addParam{ type=ULib.cmds.PlayersArg }
acred:addParam{ type=ULib.cmds.NumArg, hint="Pointshop Points", ULib.cmds.round }
acred:defaultAccess( ULib.ACCESS_SUPERADMIN )
acred:help( "Gives the target(s) Pointshop points." )

------------------------------ setpspoint ------------------------------
function ulx.setpspoint( calling_ply, target_plys, amount )

    for i=1, #target_plys do
        target_plys[i].PS_Points = amount
        target_plys[i]:PS_GivePoints(0)
        target_plys[i]:PS_Notify( "An admin set your points to "..amount.." points" )
        --target_plys[ i ]:AddCredits(amount)
    end

    ulx.fancyLogAdmin( calling_ply, true, "#A set points #T #i Points", target_plys, amount )
end
local setpspoint = ulx.command("TTT Fun", "ulx setpspoint", ulx.setpspoint, "!setpspoint")
setpspoint:addParam{ type=ULib.cmds.PlayersArg }
setpspoint:addParam{ type=ULib.cmds.NumArg, hint="Pointshop Points", ULib.cmds.round }
setpspoint:defaultAccess( ULib.ACCESS_SUPERADMIN )
setpspoint:help( "Sets the target(s) Pointshop points." )

------------------------------ website_getweapon ------------------------------

function ulx.website_getweapon( calling_ply, weapon )
    if !IsValid(calling_ply) then return end

    if( calling_ply.lastredeemWep and (calling_ply.lastredeemWep + 30) > CurTime() ) then
        calling_ply:PS_Notify( "You need to wait two minutes between redeems" )
        return
    end
    if specialRound and specialRound.isSpecialRound then
        calling_ply:PS_Notify('Special round, getweapon locked!')
        return false
    end
    calling_ply.lastredeemWep = CurTime()


    fun_api.get_person_weapons( calling_ply )

     local ownedWeapons = ""

        for _, t in ipairs( calling_ply.weapons ) do

            ownedWeapons = ownedWeapons.."\n"..t

            if t == weapon then
                if not t then continue end
                calling_ply:PS_Notify( "Delivered "..weapon )
                calling_ply:PS_GiveItem( weapon )
                calling_ply:PS_EquipItem( weapon )
                print("Success, Gave "..weapon.." to "..calling_ply:Nick())
                return
            end
        end
        local message = "===================\nYour weapons:\n==================="..ownedWeapons.."\n==================="
        if ownedWeapons == "" then
            message = "I cant find weapons you own in the database. If you just bought a weapon it can take up to 2 hours to be visible here."
        end
        calling_ply:PrintMessage( HUD_PRINTTALK,message)


end
local website_getweapon = ulx.command( "My Items", "ulx getweapon", ulx.website_getweapon, "!getweapon" )
website_getweapon:addParam{  type=ULib.cmds.StringArg, hint='weaponID'}
website_getweapon:defaultAccess( ULib.ACCESS_ALL )
website_getweapon:help( "Gives you your weapons. Type in the pointshop ID. To see your weapons, type !showweapons" )

------------------------------ website_getweapon ------------------------------
function ulx.website_showweapons( calling_ply )
    if !IsValid(calling_ply) then return end

   if( not calling_ply.lastredeemWeps or (calling_ply.lastredeemWeps + 30) < CurTime() ) then
       fun_api.get_person_weapons( calling_ply, true )
       return
    end
    timer.Simple(1,function()
        if not calling_ply.weapons or table.Count(calling_ply.weapons) < 1 then
            calling_ply:PrintMessage( HUD_PRINTTALK, "Doesnt look like you have weapons, try again in 2 minutes")
        else


                    calling_ply:PrintMessage( HUD_PRINTTALK,"================\nYOUR WEAPONS:\n================")
                    for _,w in ipairs( calling_ply.weapons ) do
                        calling_ply:PrintMessage( HUD_PRINTTALK,w)
                    end
                    calling_ply:PrintMessage( HUD_PRINTTALK,"================")


        end
    end)


end
local website_showweapons = ulx.command( "My Items", "ulx showweapons", ulx.website_showweapons, "!showweapons" )
website_showweapons:defaultAccess( ULib.ACCESS_ALL )
website_showweapons:help( "Shows you your weapons" )


------------------------------ REDEEM ------------------------------
function ulx.redeem( calling_ply )

    if !IsValid(calling_ply) then  print("Nah doesnt work  though console") return end
    if( calling_ply.lastredeem and (calling_ply.lastredeem + 120) > CurTime() ) then
        calling_ply:PS_Notify( "You need to wait two minutes between redeems" )
        return
    end
    calling_ply.lastredeem = CurTime()

    local ply = calling_ply

    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="get_extra_points",['steam']= ply:SteamID() },
    function(data)
        print(data)
        local hadpoints = tonumber(data)
        if not IsValid(ply) then return end

        timer.Destroy("ttt_fun_api_nag_points"..ply:SteamID())

        if hadpoints == "" then hadpoints = 0 end
        if (hadpoints and tonumber(hadpoints) > 0) then

            ply:PS_GivePoints( tonumber(hadpoints))
            ply:PS_Notify( "You redeemed "..hadpoints.." points!" )
            print("Donation points: "..hadpoints.." to "..ply:Nick())
            fun_api.save_user_data( ply )

            http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                ['action']="reset_extra_points",['steam']= ply:SteamID() })

        else
            ply:PS_Notify( "You don't have any points to redeem" )
        end

    end);



end
local redeem = ulx.command( "My Items", "ulx redeem", ulx.redeem, "!redeem" )
redeem:defaultAccess( ULib.ACCESS_ALL )
redeem:help( "Redeem your points" )

------------------------------ REDEEM_WEAPONPOINTS ------------------------------
function ulx.redeem_weaponpoints( calling_ply )

    if !IsValid(calling_ply) then  print("Nah doesnt work  though console") return end
    if( calling_ply.lastredeemW and (calling_ply.lastredeemW + 120) > CurTime() ) then
        calling_ply:PS_Notify( "You need to wait two minutes between redeems" )
       -- return
    end
    calling_ply.lastredeemW = CurTime()

    local ply = calling_ply


    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
        ['action']="get_weapon_points",['steam']= ply:SteamID() },
        function(data)
            print(data)
            local hadpoints = tonumber(data)
            if not IsValid(ply) then return end

            timer.Destroy("ttt_fun_api_nag_weaponpoints"..ply:SteamID())

            if hadpoints == "" then hadpoints = 0 end
            if (hadpoints and tonumber(hadpoints) > 0) then

                    ply:PS_GivePoints( tonumber(hadpoints))
                    ply:PS_Notify( "You redeemed "..hadpoints.." points!" )
                    print("Weapon points: "..hadpoints.." to "..ply:Nick())
                    fun_api.save_user_data( ply )


                http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                    ['action']="reset_weapon_points",['steam']= ply:SteamID()})

            else
                ply:PS_Notify( "You don't have any points to redeem" )
            end

        end);


end
local redeem_weaponpoints = ulx.command( "My Items", "ulx redeem_weaponpoints", ulx.redeem_weaponpoints, "!redeem_weaponpoints" )
redeem_weaponpoints:defaultAccess( ULib.ACCESS_ALL )
redeem_weaponpoints:help( "Redeem your points from weapons" )

------------------------------ website_redeemwep ------------------------------

    ------------------------------ GET FULLLOGS ------------------------------
    local todayDateAndSo = os.date("%m-%d-%y")
    function ulx.fullogs( calling_ply, date )

        local epoch

        if IsValid(calling_ply) then
            if not date then date =  os.date("%m-%d-%y") end
            local m, d, Y = date:match("(%d+)-(%d+)-(%d+)")
            epoch = os.time{year=Y, month=m, day=d}
            local zeromdy = string.format("%02d-%02d-%04d", m, d, Y)
            --return zeromdy == os.date('%m-%d-%Y', epoch)
            date = os.date('%m-%d-%y', epoch)
        else
           -- epoch
            epoch = tonumber(date)
        end

        local filename = ulx.RandomString(15)
        local filename2 = os.date( "data/ulx_logs/" .. "%m-%d-%y" .. ".txt", epoch )

        if not file.Exists( filename2, "GAME" ) then
            print("DOESNT EXIST")
            return
        end

        http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
            ['action']="post_fullogs",['server']= tostring(server_id), ['date'] = tostring(epoch), ['filename'] = filename,
            ['logs'] = tostring(gb_json.base64enc(file.Read(  os.date( "ulx_logs/" .. "%m-%d-%y" .. ".txt", epoch ) ))) },
            function(data)
                if IsValid(calling_ply) then
                    print("ulx.showWebsite("..data..")")
                    calling_ply:SendLua("ulx.showWebsite('"..data.."')");
                end
            end);
        print(filename)
    end
    local fullogs = ulx.command( "TTT Admin", "ulx fullogs", ulx.fullogs, "!fullogs" )
    fullogs:defaultAccess( ULib.ACCESS_ADMIN )
    fullogs:addParam{  type=ULib.cmds.StringArg, hint=todayDateAndSo}
    fullogs:help( "Get the full logs from the server from any moment" )

    ulx.RandomString = function( length)

        local all = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        local e = ""

        for i = 1, tonumber(length) do
            local r = math.random(#all)
            e = e.. string.sub(all, r, r)
        end
        return e
    end