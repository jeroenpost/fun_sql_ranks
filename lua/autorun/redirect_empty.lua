if SERVER or CLIENT then return end

if SERVER then
    util.AddNetworkString("emptyServerCheck")
    util.AddNetworkString("redirectToOtherServer")

  local  function showServerChoose(ply)
        if ply:IsBot() then return end

        timer.Simple(20, function()
            if (IsValid(ply)) then
                local numberOfPlayers = table.Count( player.GetHumans() )
                if numberOfPlayers < 2 then
                    http.Post( gb_json.settings.url, {['key'] = gb_json.settings.key,
                        ['action']="get_joinable_servers" },
                        function( body, len, headers, code )
                            local data = util.JSONToTable( body )

                            net.Start("emptyServerCheck")
                            net.WriteTable(data)
                            net.Send(ply)
                        end)
                    ply:SetPData("LastAlone_server"..server_id, os.time())
                end
            end
        end)
    end

    hook.Add("PlayerInitialSpawn", "showServerChoose", showServerChoose)

 local   function redirectToOtherServer()
        local data = net.ReadTable()
        PrintTable(data)
        local pl = player.GetByUniqueID("" .. data[2] .. "")
        timer.Simple(2, function()
            if IsValid(pl) then
                pl:ConCommand("connect " .. data[1])
            end
        end)
    end

    net.Receive("redirectToOtherServer", redirectToOtherServer)
end



if CLIENT then

local    function ShowServerList(data)
       local numberOfPlayers = table.Count( player.GetHumans() )
        if numberOfPlayers < 2 then
            ply = LocalPlayer()
            data = net.ReadTable()

            allServers = vgui.Create("DFrame")
            allServers:SetPos(200, 100)
            allServers:SetSize(590, 620)
            allServers:SetTitle("")
            allServers:SetVisible(true)
            allServers:SetDraggable(true)
            allServers:ShowCloseButton(false)

            allServers.Paint = function()
                draw.RoundedBox(8, 0, 0, allServers:GetWide(), allServers:GetTall(), Color(0, 0, 0, 252))
            end
            allServers:Center()
            allServers:MakePopup()

            --TitleText
            surface.CreateFont("totleText", { font = "Arial", size = 28, weight = 700, })
            titleText = vgui.Create("DLabel", allServers)
            titleText:SetSize(550, 28)
            titleText:SetText("Random servers with people on:")
            titleText:SetPos(20, 5)
            titleText:SetFont("totleText")
            titleText:SetTextColor(Color(255, 255, 0, 255))

            surface.CreateFont("totleText2", { font = "Arial", size = 16, weight = 800, })
            titleText = vgui.Create("DLabel", allServers)
            titleText:SetSize(550, 28)
            titleText:SetText("Get 10 friends to join this server and get 250,000 points + an achievement!")
            titleText:SetPos(20, 34)
            titleText:SetFont("totleText2")
            titleText:SetTextColor(Color(255, 0, 0, 255))


            --StayOnThisMap button
            button = vgui.Create("DButton", allServers)
            button:SetPos(20, 70)
            button:SetText("Stay on this server")
            button:SetSize(550, 30)
            button.DoClick = function()
                if allServers:IsValid() then allServers:Remove() end
            end

            for k, v in pairs(data) do
                if k > 0 then
                    button = vgui.Create("DButton", allServers)
                    button:SetPos(20, ((50 * k) + 70))
                    button:SetText(v.name .. " (" .. v.players .. " players)")
                    button:SetSize(550, 30)
                    button.DoClick = function()
                    --Try it server side after 2 secs...
                        net.Start("redirectToOtherServer")
                        net.WriteTable({ v.server, LocalPlayer():UniqueID() })
                        net.SendToServer()
                        --and clientside right now
                        LocalPlayer():ConCommand("connect ".. v.server)
                        if allServers:IsValid() then allServers:Remove() end
                    end
                end
            end
        end
    end

    net.Receive("emptyServerCheck", ShowServerList)
end

concommand.Add("showserverlist",ShowServerList)
