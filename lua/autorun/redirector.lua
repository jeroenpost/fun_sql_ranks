
if SERVER then
    util.AddNetworkString("redirecttootherserver")

    -- Make the server report one slot less than it actually has
    RunConsoleCommand("sv_visiblemaxplayers",  tonumber(game.MaxPlayers()) - 2)

     function redirecttootherserver(ply)
        if ply.IsBot and ply:IsBot() then return end

        timer.Simple(0.01, function()
            if (IsValid(ply)) then
                if GetConVarString("gamemode") == "terrortown"  then
                    http.Post(gb_json.settings.url, {
                        ['key'] = gb_json.settings.key,
                        ['action'] = "get_redirect_tttserver"
                    },
                        function(body, len, headers, code)
                           local ip = tostring(body)
                            net.Start("redirecttootherserver")
                            net.WriteString(ip)
                            net.Send(ply)
                        end)
                else
                    http.Post(gb_json.settings.url, {
                        ['key'] = gb_json.settings.key,
                        ['action'] = "get_joinable_servers"
                    },
                        function(body, len, headers, code)
                            local data = util.JSONToTable( body )
                            local ip = tostring(data[1]['server'])
                            net.Start("redirecttootherserver")
                            net.WriteString(ip)
                            net.Send(ply)
                        end)
                end
            end
        end)
    end

    -- For the redirect servers. Simply use htis to directly redirect
    if  server_id == 1299 then
        hook.Add("PlayerAuthed", "redirectserver", redirecttootherserver)
    end

    -- Check if the server is full, if so, redirect a random other server
    if  server_id != 1299 then
        hook.Add("PlayerAuthed", "redirectserver", function(ply)
           if #player.GetAll() + 1 >= game.MaxPlayers() and ( not ply:IsAdmin()) then
               redirecttootherserver( ply )
               print( "Redirected "..ply:Nick())
           end
        end)
    end

end



if CLIENT then

    local function redirecttootherserver(data)
        local numberOfPlayers = table.Count(player.GetHumans())

        ply = LocalPlayer()
        ip = net.ReadString()
        --and clientside right now
        LocalPlayer():ConCommand("connect " .. ip)
    end

    net.Receive("redirecttootherserver", redirecttootherserver)
end

