/*
	Hooks:
	FIGD_Mounted(item) --Called when a workshop item is mounted (item = {id = workshopId, url = gmaDownloadUrl, name = titleOfItem, size = sizeOfItem})
	FIGD_AllMounted(items) --Called when all workshop items are mounted, called even when some failed to load, but tried all of them.
*/

--if VERSION < 160219 then return end

hook.Add("FIGD_AllMounted", "NotifyPlayer", function()
	chat.AddText(Color(255,255,255), "[", Color(0, 200, 200), "FIGD", Color(255,255,255), "] ", Color(200,200,200), "Successfully mounted every workshop addons!") 
end)

--DO NOT EDIT THIS FILE UNLESS YOU KNOW WHAT YOU'RE DOING

FIGD = FIGD or {}
FIGD.Directory = "ingame_downloads"
FIGD.ConfigFileName = "fanglis_ingame_downloads_data.txt"
FIGD.Running = false
FIGD.CurrentJob = "Idle"
FIGD.CurrentJobSubtitle = "ItemName"
FIGD.CurrentJobPercentage = 0
FIGD.AssumeInternetSpeed = 1.5 * 1000 * 1000 -- 1.5 MB/s

file.CreateDir(FIGD.Directory)

function FIGD.Print(str)
	MsgC(Color(255,255,255), "[", Color(0, 200, 200), "FIGD", Color(255,255,255), "] ", Color(200,200,200), str, "\n")
end

function FIGD.ChatPrint(str)
	chat.AddText(Color(255,255,255), "[", Color(0, 200, 200), "FIGD", Color(255,255,255), "] ", Color(200,200,200), str, "\n")
end

function FIGD.HandleError(sErr)
	MsgC(Color(255,255,255), "[", Color(220,20,20), "FIGD ERROR", Color(255,255,255), "] ", Color(200,200,200), sErr, "\n")
	chat.AddText(Color(255,255,255), "[", Color(220,20,20), "FIGD ERROR", Color(255,255,255), "] ", Color(200,200,200), sErr, "\n")
	ErrorNoHalt("[FIGD] ERROR: "..sErr.."\n")
end

function FIGD.NiceSize(size)
	if size < 1000 then
		return math.Round(size,2).." bytes"
	else
		local kb = size/1000
		if kb < 1000 then
			return math.Round(kb,2).." KB"
		else
			return math.Round(kb/1000,2).." MB"
		end
	end
end

function FIGD.RequestFiles()
	RunConsoleCommand("FIGD_request_items")
end



function FIGD.GetConfig()
	local currentConfig
	local contents = file.Read(FIGD.ConfigFileName,"DATA")
	if contents and #contents > 0 then
		currentConfig = von.deserialize(contents)
	else
		currentConfig = {}
	end

	return currentConfig
end
function FIGD.SaveConfig(data)
	file.Write(FIGD.ConfigFileName, von.serialize(data))
end



function FIGD.OnFilesReceived()
	if FIGD.Running then return end
	FIGD.Items = FIGD.Items or {}

	FIGD.menuSettings = net.ReadTable()
	local itemsCount = net.ReadInt(16)

	for i=1, itemsCount do
		local id = net.ReadString()
		local url = net.ReadString()
		local name = net.ReadString()
		local size = net.ReadInt(32)
		local last_updated = net.ReadInt(32)

		--FIGD.Print("Received workshop item to auto-download: '"..name.."' ("..FIGD.NiceSize(size)..")")
		FIGD.Items[i] = {id=id, url=url, name=name, size=size, last_updated = last_updated}
	end

	if FIGD.menuSettings["openMenu"] then
		FIGD.OpenMenu()
	else
		FIGD.MountAllItems()
	end
end
net.Receive("FIGD_Files", FIGD.OnFilesReceived)


function FIGD.CheckMountedItems()
	local bAllMounted = true
	for k,v in pairs(FIGD.Items) do
		if not v.mounted then
			bAllMounted = false
			break
		end
	end
	if bAllMounted then
		hook.Call("FIGD_AllMounted", GAMEMODE, FIGD.Items)
	end
end


function FIGD.MountAllItems()
	FIGD.Running = true
	FIGD.Thread = coroutine.create(function()
		local config = FIGD.GetConfig()

		for k,item in pairs(FIGD.Items) do
			if item.mounted then 
				continue
			end
			if item.wantDownload == false then
				continue
			end

			local path
			if file.Exists("cache/workshop/"..item["url"]..".cache", "GAME") then
				path = "cache/workshop/"..item["url"]..".cache"
			end

			local needUpdate = false 
			local isUpdate = false

			if not config[item.id] or not config[item.id].last_updated or config[item.id].last_updated < item["last_updated"] then
				needUpdate = true
			end
			if config[item.id] and config[item.id].lastuplast_updateddated and config[item.id].last_updated < item["last_updated"] then
				isUpdate = true
			end

			if not path or needUpdate then
				--Not downloaded yet or needs to be updated
				FIGD.DownloadItem(item, function()
					FIGD.Items[k].mounted = true
					FIGD.CheckMountedItems()
				end, isUpdate)

				coroutine.yield()
			else
				item["filename"] = path
				FIGD.MountItem(item, function()
					FIGD.Items[k].mounted = true
					FIGD.CheckMountedItems()
				end)

				coroutine.yield()
			end
		end

		FIGD.Running = false
	end)
	coroutine.resume(FIGD.Thread)
end


function FIGD.DownloadItem(item, callback, isUpdate)
	FIGD.Downloading = FIGD.Downloading or {}
	FIGD.Downloading[item.id] = item

	FIGD.CurrentJob = (isUpdate and "Updating " or "Downloading ")..FIGD.NiceSize(item.size)
	FIGD.CurrentJobSubtitle = item.name
	FIGD.CurrentJobPercentage = 0
	timer.Create("DownloadProgressBar", 0, .1, function()
		FIGD.CurrentJobPercentage = math.Clamp(FIGD.CurrentJobPercentage + (FIGD.AssumeInternetSpeed / item.size / 10), 0, .98)
	end)

	FIGD.Print("Now downloading item '"..item.name.."' ("..FIGD.NiceSize(item.size)..")")
	steamworks.Download(item.url, true, function(name)
		item["filename"] = name

		timer.Remove("DownloadProgressBar")
		FIGD.Print("Downloaded item '"..item.name.."', now mounting...")
		FIGD.Downloading[item.id] = false


		--Save configuration
		local config = FIGD.GetConfig()
		if not config[item.id] then
			config[item.id] = {}
		end
		config[item.id].last_updated = item.last_updated
		FIGD.SaveConfig(config)


		FIGD.MountItem(item, callback)
	end)
end



function FIGD.MountItem(item, callback)
	FIGD.CurrentJob = "Mounting..."
	FIGD.CurrentJobSubtitle = item.name
	FIGD.CurrentJobPercentage = 0

	timer.Simple(1, function()
		--Wait a little so DrawOverlay can catch up

		local bSuccess, files = game.MountGMA(item["filename"])
		FIGD.CurrentJobPercentage = 1

		if bSuccess then
			hook.Call("FIGD_Mounted", GAMEMODE, item)
			--FIGD.Print("Successfully mounted workshop item '"..item.name.."'")
		else
			FIGD.HandleError("Failed to mount workshop item '"..item.name.."' ("..tostring(item.filename).."), please check the console for errors. Rejoining might fix it.")
			if item.filename:find(".txt") then
				file.Delete(item.filename)
			end
		end

		if coroutine.status(FIGD.Thread) != "running" then
			coroutine.resume(FIGD.Thread)
		end

		if callback then callback() end
	end)
end


surface.CreateFont("FIGDFont", {
	font = "Roboto",
	weight = 500,
	size = 27,
})
surface.CreateFont("FIGDFontSub", {
	font = "Roboto",
	weight = 500,
	size = 18,
})


surface.CreateFont("FIGDWindowFontTitle", {
	font = "Roboto",
	weight = 500,
	size = 20,
})
surface.CreateFont("FIGDWindowFontText", {
	font = "Roboto",
	weight = 500,
	size = 17,
})
surface.CreateFont("FIGDWindowButtonFont", {
	font = "Roboto",
	weight = 500,
	size = 18,
})
surface.CreateFont("FIGDCheckBoxFont", {
	font = "Arial",
	weight = 500,
	size = 25,
})



function FIGD.OpenAddonSelectionMenu()
	if FIGD.SelectionMenuPanel and IsValid(FIGD.SelectionMenuPanel) then
		FIGD.SelectionMenuPanel:Close()
	end

	local w,h = 400, 600
	local settings = FIGD.menuSettings

	local menu = vgui.Create("DFrame")
	FIGD.SelectionMenuPanel = menu
	menu:SetTitle("")
	menu:ShowCloseButton(false)
	menu:SetSize(w, h)
	menu:Center()
	menu.Paint = function(self, w, h)
		surface.SetDrawColor(50, 50, 50, 180)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(30, 30, 30, 220)
		surface.DrawRect(3, 3, w-6, h-6)

		--Line
		surface.SetDrawColor(255,255,255)
		surface.DrawRect(3, 43, w-6, 1)

		--Title
		draw.SimpleText("Select which addons to download", "FIGDWindowFontTitle", 13, 13, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
	end
	menu:MakePopup()

	--Close button
	if settings["showCloseButton"] then
		closeButton = vgui.Create("DButton")
		closeButton:SetText("X")
		closeButton:SetFont("FIGDWindowFontText")
		closeButton:SetSize(32, 32)
		closeButton:SetPos(w - 38, 6)
		closeButton:SetParent(menu)
		closeButton:SetTextColor(Color(230, 230, 230))
		closeButton.DoClick = function()
			menu:Close()
		end

		closeButton.Paint = function(self, w, h)
		end
	end




	--Now the actual list
	mainPanel = vgui.Create("DScrollPanel", menu)
	mainPanel:SetPos(10, 55)
	mainPanel:SetSize(w-20, h-105)
	mainPanel.Paint = function() end
	local VBar = mainPanel.VBar

	VBar.Paint = function(self, w, h) 
		surface.SetDrawColor(0, 0, 0, 160)
		surface.DrawRect(0, 10, w, h-20)
	end

	VBar.btnGrip.Paint = function(self, w, h)
		surface.SetDrawColor(80, 80, 80, 240)
		surface.DrawRect(4, 0, w-8, h)
	end

	VBar.btnUp.Paint = function() end
	VBar.btnDown.Paint = function() end

	local config = FIGD.GetConfig()
	local addedItemsCount = 0
	for k,v in pairs(FIGD.Items) do
		local name = v.name
		local size = FIGD.NiceSize(v.size)
		local mounted = v.mounted
		local id = v.id
		local downloaded = file.Exists("cache/workshop/"..v["url"]..".cache", "GAME")


		local itemPanel = vgui.Create("DPanel", mainPanel)
		itemPanel:SetSize(w-55, 30)
		itemPanel:SetPos(5, 10+30*(addedItemsCount)+10*(addedItemsCount))
		itemPanel.Paint = function(self, w, h)
			surface.SetDrawColor(150, 150, 150, 220)
			surface.DrawRect(0, 0, w, 1)
			surface.DrawRect(0, h-1, w, 1)
			surface.DrawRect(0, 0, 1, h)
			surface.DrawRect(w-1, 0, 1, h)
		end

		local pW, pH = itemPanel:GetSize()

		local checkBox = vgui.Create("DCheckBox", itemPanel)
		checkBox:SetPos(7,7)
		checkBox:SetSize(17, 17)
		checkBox:SetValue(true)
		FIGD.Items[k].wantDownload = true
		checkBox.ItemKey = k
		if downloaded then
			checkBox:SetValue(true)
			checkBox:SetDisabled(true)
			checkBox:SetTooltip("You already have this item downloaded!")

		elseif config[v.id] and config[v.id].wantDownload then
			checkBox:SetValue(config[v.id].wantDownload == 1)
			FIGD.Items[k].wantDownload = config[v.id].wantDownload == 1
		end

		checkBox.OnChange = function(self, val)
			FIGD.Items[k].wantDownload = val
		end

		checkBox.OnCursorEntered = function(self)
			self.Highlight = true
		end
		checkBox.OnCursorExited = function(self)
			self.Highlight = false
		end

		checkBox.Paint = function(self, w, h) 
			surface.SetDrawColor(32, 32, 32, 255)
			surface.DrawRect(0, 0, w, h)

			if ( self:GetChecked() ) then
				draw.SimpleText("✔", "FIGDCheckBoxFont", w/2, h/2-1, Color(230, 230, 230), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

				if ( self:GetDisabled() ) then
					surface.SetDrawColor(100,100,100,220)
					surface.DrawRect(0,0,w,h)
				end
			else
				if ( self:GetDisabled() ) then
					surface.SetDrawColor(100,100,100,220)
					surface.DrawRect(0,0,w,h)
				end
			end

			--Highlight
			if not self:GetDisabled() and self.Highlight then
				surface.SetDrawColor(0, 151, 251, 255)
				surface.DrawRect(0, 0, w, 1)
				surface.DrawRect(0, h-1, w, 1)
				surface.DrawRect(0, 0, 1, h)
				surface.DrawRect(w-1, 0, 1, h)
			end
		end


		local itemName = vgui.Create("DLabel", itemPanel)
		itemName:SetText(name)
		itemName:SetFont("FIGDWindowFontText")
		itemName:SetPos(35, 7)
		itemName:SetWidth(w-180)


		surface.SetFont("FIGDWindowFontText")
		local offsetX = surface.GetTextSize(size)
		local itemSize = vgui.Create("DLabel", itemPanel)
		itemSize:SetText(size)
		itemSize:SetFont("FIGDWindowFontText")
		itemSize:SetPos(pW-5-offsetX, 7)
		itemSize:SetWidth(w-150)

		addedItemsCount = addedItemsCount + 1
	end

	--How to get the scrollbar active all the time.
	mainPanel.PerformLayout = function(self)
		local Wide = self:GetWide()
		local YPos = 0
		
		self:Rebuild()
		
		if self.pnlCanvas:GetTall() > self:GetTall()+1 then
			self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() )
		else
			self.VBar.Hacked = true
			self.VBar:SetUp( self:GetTall(), self:GetTall()+1 )
		end

		YPos = self.VBar:GetOffset()
			
		if ( self.VBar.Enabled ) then Wide = Wide - self.VBar:GetWide() end

		self.pnlCanvas:SetPos( 0, YPos )
		self.pnlCanvas:SetWide( Wide )
		
		self:Rebuild()
	end



	local acceptButton = vgui.Create("DButton", menu)
	acceptButton:SetPos(10, h-52)
	acceptButton:SetText("Download selected addons")
	acceptButton:SetFont("FIGDWindowButtonFont")
	acceptButton:SetTextColor(Color(255, 255, 255))
	acceptButton:SetParent(menu)
	acceptButton:SetSize(w-20, 44)

	acceptButton.Paint = function(self, w, h)
		surface.SetDrawColor(150, 150, 150, 220)
		surface.DrawRect(0, 0, w, 1)
		surface.DrawRect(0, h-1, w, 1)
		surface.DrawRect(0, 0, 1, h)
		surface.DrawRect(w-1, 0, 1, h)
	end

	acceptButton.DoClick = function()
		local config = FIGD.GetConfig()
		for k,v in pairs(FIGD.Items) do
			config[v.id] = config[v.id] or {}
			config[v.id].wantDownload = v.wantDownload and 1 or 0
		end
		FIGD.SaveConfig(config)

		FIGD.MountAllItems()
		menu:Close()
	end
end


function FIGD.OpenMenu(bNoMount)
	if FIGD.MenuPanel and IsValid(FIGD.MenuPanel) then
		FIGD.MenuPanel:Close()
	end

	local settings = FIGD.menuSettings
	settings["itemCount"] = 0
	for k,v in pairs(FIGD.Items) do
		if not file.Exists("cache/workshop/"..v["url"]..".cache", "GAME") then
			settings["itemCount"] = settings["itemCount"] + 1
		end
	end

	if settings["itemCount"] == 0 then
		--Already downloaded all of them
		if bNoMount then
			FIGD.ChatPrint("You already have all the addons downloaded & mounted!")
		else
			FIGD.MountAllItems()
		end
		return
	end


	local w,h = 400, 260

	local menu = vgui.Create("DFrame")
	FIGD.MenuPanel = menu
	menu:SetTitle("")
	menu:ShowCloseButton(false)
	menu:SetSize(w, h)
	menu:Center()
	menu.Paint = function(self, w, h)
		surface.SetDrawColor(50, 50, 50, 180)
		surface.DrawRect(0, 0, w, h)

		surface.SetDrawColor(30, 30, 30, 220)
		surface.DrawRect(3, 3, w-6, h-6)

		--Line
		surface.SetDrawColor(255,255,255)
		surface.DrawRect(3, 43, w-6, 1)

		draw.SimpleText(string.format("Welcome to %s!", settings.serverName), "FIGDWindowFontTitle", 13, 13, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
		--draw.SimpleText(string.format("Welcome to %s!", settings.serverName), "FIGDWindowFontTitle", 13, 13, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
		local startY = 57
		draw.SimpleText(string.format("You need to download %d Workshop items which contain", settings.itemCount), "FIGDWindowFontText", 13, startY, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
		draw.SimpleText("the assets needed to play on this server.", "FIGDWindowFontText", 13, startY+21, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)

		if settings["showCloseButton"] then
			draw.SimpleText("You can choose not to download them, but you might see", "FIGDWindowFontText", 13, startY+60, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
			draw.SimpleText("missing models, materials.", "FIGDWindowFontText", 13, startY+81, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)

			surface.SetFont("FIGDWindowFontText")
			local pushX = 0
			draw.SimpleText("Type ", "FIGDWindowFontText", 13, startY+120, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
			pushX = pushX + surface.GetTextSize("Type ")+2
			draw.SimpleText(settings["openMenuCommand"], "FIGDWindowFontText", 13 + pushX, startY+120, Color(0, 200, 200), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
			pushX = pushX + surface.GetTextSize(settings["openMenuCommand"])+5
			draw.SimpleText("to open this menu again.", "FIGDWindowFontText", 13 + pushX, startY+120, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
		else
			draw.SimpleText("Downloading these addons are required to play on the", "FIGDWindowFontText", 13, startY+60, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
			draw.SimpleText("server. All the downloads happen in-game!", "FIGDWindowFontText", 13, startY+81, Color(230, 230, 230), TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
		end
	end

	local buttonPaint = function(self, w, h)
		surface.SetDrawColor(150, 150, 150, 220)
		surface.DrawRect(0, 0, w, 1)
		surface.DrawRect(0, h-1, w, 1)
		surface.DrawRect(0, 0, 1, h)
		surface.DrawRect(w-1, 0, 1, h)
	end

	local acceptButton, closeButtonBig, closeButton
	acceptButton = vgui.Create("DButton")
	acceptButton:SetPos(10, h-38)
	acceptButton:SetText("Download addons now")
	acceptButton:SetFont("FIGDWindowButtonFont")
	acceptButton:SetTextColor(Color(255, 255, 255))
	acceptButton:SetParent(menu)
	acceptButton.Paint = buttonPaint
	acceptButton.DoClick = function()
		if settings["allowToSelectAddons"] then
			FIGD.OpenAddonSelectionMenu()
		else
			for k,v in pairs(FIGD.Items) do
				FIGD.Items[k].wantDownload = true
			end

			FIGD.MountAllItems()
		end
		menu:Close()
	end

	if settings["showCloseButton"] then
		closeButton = vgui.Create("DButton")
		closeButton:SetText("X")
		closeButton:SetFont("FIGDWindowFontText")
		closeButton:SetSize(32, 32)
		closeButton:SetPos(w - 38, 6)
		closeButton:SetParent(menu)
		closeButton:SetTextColor(Color(230, 230, 230))
		closeButton.DoClick = function()
			menu:Close()
		end

		closeButton.Paint = function(self, w, h)
		end


		closeButtonBig = vgui.Create("DButton")
		closeButtonBig:SetPos(w/2+10, h-38)
		closeButtonBig:SetSize(w/2-25, 30)
		closeButtonBig:SetText("Download next time")
		closeButtonBig:SetFont("FIGDWindowButtonFont")
		closeButtonBig:SetTextColor(Color(255, 255, 255))
		closeButtonBig:SetParent(menu)
		closeButtonBig.Paint = buttonPaint
		closeButtonBig.DoClick = function()
			menu:Close()
		end

		acceptButton:SetSize(w/2-20, 30)

	else
		acceptButton:SetSize(w-20, 30)
	end

	menu:MakePopup()

	--timer.Simple(5, function() menu:Close() end)
end
net.Receive("FIGDOpenMenu", FIGD.OpenMenu)


hook.Add("DrawOverlay", "DrawIngameDownloads", function()
	if not FIGD.Running or FIGD.CurrentJob == "Idle" then return end

	local w,h = 300, 120
	local x,y = ScrW()-w, ScrH()*0.1

	surface.SetDrawColor(50, 50, 50, 255)
	surface.DrawRect(x, y, w, h)

	draw.Text({text = FIGD.CurrentJob, font = "FIGDFont", pos = {x+w*.5, y+h*0.1}, xalign = TEXT_ALIGN_CENTER, color = Color(230, 230, 230)})
	draw.Text({text = #FIGD.CurrentJobSubtitle > 32 and FIGD.CurrentJobSubtitle:sub(1,32).."..." or FIGD.CurrentJobSubtitle, font = "FIGDFontSub", pos = {x+w*.5, y+h*.38}, xalign = TEXT_ALIGN_CENTER, color = Color(230, 230, 230)})

	local statusX, statusY = x+20, y+h*.65
	local statusW, statusH = w-40, y*.2
	draw.RoundedBox(4, statusX, statusY, statusW, statusH, Color(37, 78, 108, 200))

	if FIGD.CurrentJobPercentage > .02 then
		draw.RoundedBox(4, statusX, statusY, statusW * FIGD.CurrentJobPercentage, statusH, Color(10, 151, 239, 255))
	end

	draw.Text({text = tostring(math.Round(FIGD.CurrentJobPercentage*100)).."%", font = "FIGDFontSub", pos = {statusX+statusW/2, statusY+statusH/2+1}, xalign = TEXT_ALIGN_CENTER, yalign = TEXT_ALIGN_CENTER, color = Color(255, 255, 255)})
end)


hook.Add("InitPostEntity", "RequestFIGDFiles", function()
	--Let's wait with it
	timer.Simple(12, function()
		--FIGD.Print("This server uses Fangli's In-Game Downloads, which makes it available to download addons while in-game.")
		--FIGD.Print("Made by: fangli - http://steamcommunity.com/id/fangli/")
		--Please do not remove the credits.

		FIGD.RequestFiles()
	end)
end)