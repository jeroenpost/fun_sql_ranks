// client side apple

// Spawn
function player_spawn( data )
local name1 = data:ReadString()
	chat.AddText( Color( 255, 0, 255 ), "[Server] ",  Color(255,0,0,0), name1, Color( 255, 255, 255 ), " has joined in the server." )
	--surface.PlaySound( "garrysmod/save_load1.wav" )
end
usermessage.Hook("player_spawn", player_spawn)

// Disconnect
function player_disconnect( data )
local name3 = data:ReadString()
	chat.AddText( Color( 255, 0, 255 ), "[Server] ", Color(255,0,0,0), name3, Color( 255, 255, 255 ), " has left the server." )
	--surface.PlaySound( "garrysmod/save_load2.wav" )
end
usermessage.Hook("player_disconnect", player_disconnect)


// Authed
function player_authed( data )
local name3 = data:ReadString()
chat.AddText( Color( 255, 0, 255 ), "[Server] ",  Color(255,0,0,0), name3, Color( 255, 255, 255 ), " is connecting to the server." )
--surface.PlaySound( "garrysmod/save_load2.wav" )
end
usermessage.Hook("player_authed", player_authed)