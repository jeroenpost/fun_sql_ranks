
--Spawn
function FirstSpawn( ply )
	timer.Create( "server_spawn_timer_wait"..ply:UniqueID() , 3, 1, function()
	if IsValid(ply) and ply:IsPlayer() then
		colour1 = ply:Team() 
			
		spawn1 = ply:Nick()
			umsg.Start( "player_spawn")
			umsg.String(spawn1)
			umsg.Short(colour1)
			umsg.End()
		Msg("Player " .. spawn1 .. " has joined the server.\n")
	end
end)
end
  
hook.Add( "PlayerInitialSpawn", "playerInitialSpawn", FirstSpawn )

 
--Disconnect
function PlayerDisconnect( ply )
colour3 = ply:Team()
spawn3 = ply:Nick()
	umsg.Start( "player_disconnect")
	umsg.String(spawn3)
	umsg.Short(colour3)
	umsg.End()
    Msg("Player " .. spawn3 .. " has left the server.\n")

end
  
hook.Add( "PlayerDisconnected", "playerDisconnected", PlayerDisconnect )

local function PlayerConnect( name )
    local colour3 = Color(255,255,255,255)
    local spawn3 = name
    umsg.Start( "player_authed")
    umsg.String(spawn3)

    umsg.End()
    Msg("Player " .. spawn3 .. " is connecting to the server.\n")
end

hook.Add( "PlayerConnect", "PlayerConnect", PlayerConnect )
