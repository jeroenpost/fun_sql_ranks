
local function ServerHop( pl, text, teamonly )
    if (text == "!servers" or text == "!hop") then
        pl:SendLua('gui.OpenURL("http://ttt-fun.com/servers","TTT-FUN Servers")')
    end
    if (text == "!apply") then
        pl:SendLua('gui.OpenURL("http://ttt-fun.com/node/add/staff-application","TTT-FUN Servers")')
    end
    if (text == "!join" ) then
        pl:SendLua('gui.OpenURL("http://steamcommunity.com/groups/ttt-fun")')
    end
    if (text == "!donate") then
        pl:SendLua('gui.OpenURL("https://ttt-fun.com/donate","TTT-FUN Donate")')
    end
        if (text == "!discord" or text == "!ineedtotalktootherpeopleoutsideofthisserverthroughdiscord" or text == "!ts" ) then
            pl:SendLua('gui.OpenURL("https://ttt-fun.com/discord","TTT-FUN Discord")')
        end


    if( text == "!globalban") then
        pl:SendLua('gui.OpenURL("http://ttt-fun.com/adminpanel/globalbans/","TTT-FUN Global Bans")')
    end
    if( text == "!mod") then
        pl:SendLua('gui.OpenURL("http://ttt-fun.com/staff/rules/","TTT-FUN Moderator Help")')
    end
    if text == "!vcmod" or text == "!vc" then
    pl:SendLua('RunConsoleCommand("vc_open_menu", LocalPlayer():EntIndex())');
    end
    if text == "!rtv" and gb_mapvote and isfunction(gb_mapvote.rtvCheckChat) then
        gb_mapvote.rtvCheckChat()
    end

    if text == "!unequip_everything" then
        pl:PS_Unequip()
    end

    -- Blocking dead people from talking to the living

    if teamonly and GetConVarString("gamemode") == "terrortown" and pl:GetRole() == ROLE_INNOCENT and pl:Alive() and  pl:GetObserverMode() == OBS_MODE_NONE then
        if pl.IsGhost then
            if not pl:IsGhost() then
                pl:ChatPrint("Use global chat as Innocent")
                return ""
            end
        else
            pl:ChatPrint("Use global chat as Innocent")
            return ""
        end
    end
    if not teamonly and GetConVarString("gamemode") == "terrortown" and( not pl:Alive() or pl:IsSpec()) and GetRoundState() == ROUND_ACTIVE  then
        pl:ChatPrint("You cant talk to the living")
        return ""
    end
end
hook.Add( "PlayerSay", "Chat", ServerHop )