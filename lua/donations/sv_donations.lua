
fun_donations.init = function()
    print("Loading Fun Donationsystem")
    fun_donations.serverType = GetConVarString("gamemode")
    fun_donations.actions = {}
    fun_donations.getActions()
    fun_donations.setUlxCommand()
end

fun_donations.getActions = function()
    include "donations/actions/darkrp_money.lua"
    include "donations/actions/pointshop_item.lua"
    include "donations/actions/pointshop_points.lua"
    include "donations/actions/rank.lua"
end

fun_donations.getPlayerDonationItems = function(ply, redeemUnclaimed)
    ply.hasUnclaimedItems = false
    if ply:IsBot() then return end
    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "player_getdonationitems",
        ['steam']      = tostring(ply:SteamID())
    }
    local redeemUnclaimed = redeemUnclaimed
    if redeemUnclaimed then
        print(ply:Nick().." is claiming unclaimed doantionitems")
    end
    http.Post( gb_json.settings.url, sendTable ,function(data)
        -- Player gone? lets stop
        if not IsValid(ply) then return end
       -- print("Getting doantion items for "..ply:Nick()..": "..data)
        local items =  util.JSONToTable( data )

        if not istable(items) then
            return
        end

        for k,v in pairs( items ) do
            -- Check servertype
            local item = v
            if item.servertype ~= fun_donations.serverType and item.servertype ~= "all" then
                continue
                end
                if item.endtime and tonumber(item.endtime) > 0 and tonumber(item.endtime) < os.time() then
                    continue
                end
                if not item.action or not fun_donations.actions then
                    return
                end
            -- Check singleuse
            if item.singleuse and tonumber(item.singleuse) > 0 then
               if item.claimed and tonumber(item.claimed) > 0 then
                   continue
               end
                   ply.hasUnclaimedItems = true
                   item.singleusebool = true
            end

            if isfunction( fun_donations.actions[item.action] ) then

              if (not item.singleusebool or redeemUnclaimed) then
                  --PrintTable(item)

                     -- print(ply:Nick().." is claiming "..item.action.." "..item.arguments)

                   local itemdelivered = fun_donations.actions[item.action]( ply, item.arguments)
                    if itemdelivered and item.singleusebool then
                        fun_donations.itemDelivered( ply, item.id)
                    end
               end
            else
                print("====WARNING==== ITEM BROKEN")
                PrintTable( item )
            end

        end

            if ply.hasUnclaimedItems and not redeemUnclaimed then
                -- Lets spam
                ply:ChatPrint("You have unclaimed donationitems! Type !getdonationitems to redeem your items")
                timer.Create("fun_donate_sent_singleuse"..ply:SteamID(),120,10,function()
                    if not IsValid(ply) then return end
                    ply:ChatPrint("You have unclaimed donationitems! Type !getdonationitems to redeem your items")
                end)
            end
            if redeemUnclaimed then
                fun_api.save_user_data( ply, true )
                timer.Destroy("fun_donate_sent_singleuse"..ply:SteamID())
                if ply.hasUnclaimedItems then
                    ply:PS_Notify( "You have redeemed all your donation items" )
                else
                    ply:PS_Notify( "You don't have any items to redeem. Just donated? Rejoin the server :-)" )
                end
            end

    end);

end

fun_donations.itemDelivered = function(ply, itemid)
    local sendTable = {
        ['key']     = gb_json.settings.key,
        ['action']  = "player_setdonationitemused",
        ['steam']      = tostring(ply:SteamID()),
        ['itemid']      = tostring(itemid )
    }

    http.Post( gb_json.settings.url, sendTable ,function(data)
    end)
end

-- ULX command
fun_donations.setUlxCommand = function()
    if not ulx then return end
    function ulx.getdonationitems( calling_ply )

        if not IsValid(calling_ply) then  print("Nah doesnt work  though console") return end
        if( calling_ply.lastredeemD and (calling_ply.lastredeemD + 120) > CurTime() ) then
            calling_ply:PS_Notify( "You need to wait two minutes between redeems" )
            -- return
        end
        calling_ply.lastredeemD = CurTime()

        fun_donations.getPlayerDonationItems( calling_ply, true )

    end
    local getdonationitems = ulx.command( "Redeem Donation Items", "ulx getdonationitems", ulx.getdonationitems, "!getdonationitems" )
    getdonationitems:defaultAccess( ULib.ACCESS_ALL )
    getdonationitems:help( "Redeem all your donationitems" )
end

hook.Add("InitPostEntity","fun_donations_init", fun_donations.init)
hook.Add("PlayerInitialSpawn","fun_donations_playerspawn", fun_donations.getPlayerDonationItems)