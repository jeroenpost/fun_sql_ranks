fun_donations.actions["pointshop_item"] = function(ply, item)
    if IsValid(ply) and isfunction( ply.PS_GiveItem ) then
        if not ply:PS_HasItem( item ) then
            ply:PS_GiveItem( item )
            ply:PS_EquipItem( item )
        end
        return true
    end
    return false
end