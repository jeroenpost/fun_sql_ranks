fun_donations.actions["pointshop_points"] = function(ply, points)
    if IsValid(ply) and isfunction( ply.PS_GivePoints ) then
        ply:PS_GivePoints( tonumber(points))
        ply:PS_Notify( "You redeemed "..points.." points!" )
        ply:ChatPrint("You redeemed "..points.." points!")
        return true
    end
    return false
end