if SERVER then
    AddCSLuaFile()
end

recruiters = {}

-- Recruiter rewards
recruiters.singlereward  = 5000
recruiters.rewards = {}
recruiters.rewards[3]   = 25000
recruiters.rewards[10]  = 150000
recruiters.rewards[25]  = 500000
recruiters.rewards[50]  = 1000000
recruiters.rewards[100] = 1500000
recruiters.rewards[250] = 3500000
recruiters.rewards[500] = 5500000
recruiters.rewards[1000] = 15000000

-- Joiner rewards
recruiters.jrewards = {}
recruiters.jrewards[1] = "8000"
recruiters.jrewards[2] = "6000"
recruiters.jrewards[3] = "4000"
recruiters.jrewards[4] = "2500"
recruiters.jrewards[5] = "1000"
