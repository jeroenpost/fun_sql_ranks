recruiters.debugEnabled = false

function recruiters.getData(var)
    return file.Read("recruiters."..var..".txt", "DATA") or CurTime()
end

function recruiters.setData(var, data)
    file.Write("recruiters."..var..".txt", data)
end

recruiters.isServerEmpty = function()
    local lasttime = recruiters.getData("lasttime")

    if recruiters.debugEnabled then
        print("===REQ== Last time: "..lasttime.." ".. recruiters.getData("recruiter"))
        print("===REQ== New recruiter in "..(lasttime + 300 - os.time()))
    end

    if ((lasttime + 300) < os.time() ) then
        local players = table.Count(player.GetHumans())
        if recruiters.debugEnabled then
            print("===REQ== Number of players: "..players)
        end
        if players < 2 then
            return true
        end
    end
    print("===REQ== Server is NOT empty")


    return false
end

recruiters.resetRecruiter = function()
    if recruiters.debugEnabled then
        print("===REQ== Recruiter reset")
    end
    local recruiter = recruiters.getRecruiter()
    if recruiter and IsValid(recruiter) then
        recruiters.setRecruiter( recruiter, false )
    elseif recruiters.isServerEmpty() then
        if recruiters.debugEnabled then
            print("===REQ== Looking for new recruiter")
        end
        for k, v in pairs(player.GetHumans()) do
            if IsValid(v) and (v.IsNPC and not v:IsNPC()) then
                recruiters.setRecruiter(v, true)
            end
        end
    end
end

recruiters.getRecruiter = function()

    if ( (tonumber(recruiters.getData("lasttime")) + 300) > os.time() )  then
        local recruiter_steamid = recruiters.getData("recruiter")
        for k, v in pairs(player.GetHumans()) do
            if IsValid(v) and v:SteamID() == recruiter_steamid and (os.time() - v:GetPData("Recruiter") < 300) then
                if recruiters.debugEnabled then
                    print("===REQ== GOT RECRUITER FROM GETRECRUITER")
                end
                return v
            end
        end
    end
    return false
end

recruiters.rewardRecruiter = function( recruiter )
    if not IsValid(recruiter) then return end
    hook.Call("recrouter.recruited", GAMEMODE, recruiter)
    if recruiters.debugEnabled then
        print("===REQ== Rewarded player: "..recruiter:Nick())
    end
    recruiter:SetNWString("recruiter.alertText", "Player Joined!! "..recruiters.singlereward.." Rewarded")
    timer.Create("recruiter.alertText",6,1,function() if IsValid(recruiter) then recruiter:SetNWString("recruiter.alertText","") end end)
    if server_id != 999 and recruiter.PS_GivePoints and recruiter.PS_Points then
        recruiter:PS_GivePoints(recruiters.singlereward)
        recruiter:PS_Notify( "You earned "..recruiters.singlereward.." points!" )
    end
end

recruiters.rewardJoiner = function( ply )
    if not IsValid(ply) then return end
    local players = tonumber(table.Count(player.GetHumans())) -1
    local reward = recruiters.jrewards[players]

    if reward then
        local playy = ply
        timer.Simple(10,function()
            if not IsValid(playy) then return end
            reward = tonumber(reward or 0)
            playy:SetNWString("recruiter.alertText", "You are nr. "..players.." joiner! "..reward.." rewarded!" )
            timer.Create("recruiter.alertText"..playy:SteamID(),6,1,function() if IsValid(playy) then
                playy:SetNWString("recruiter.alertText","")
                    playy:ConCommand("recruiter_closebar");
                end
            end)
            if server_id != 999 and playy.PS_GivePoints then
            playy:PS_GivePoints(reward)
            playy:PS_Notify( "You got "..reward.." points!" )
            end


            playy:ConCommand("recruiter_bar");
        end)
    end
end

recruiters.setRecruiter = function(recruiter, first)
    if not IsValid(recruiter) or not recruiter.IsNPC or recruiter:IsNPC() or not recruiter.IsBot or recruiter:IsBot() then return end

    recruiter:SetPData("Recruiter", os.time())
    recruiter:SetNWBool("Recruiter",true)
    recruiters.setData("recruiter",recruiter:SteamID())
    recruiters.setData("lasttime",os.time())

    if recruiters.debugEnabled then
         print("===REQ== Recruiter updated "..recruiter:Nick())
    end

    if first then
        if recruiters.debugEnabled then
             print("===REQ== Recruiter set FIRSTTIME "..recruiter:Nick())
        end
        recruiter:SetPData("recruiter.recruited", 0)
        recruiter:SetNWInt("recruiter.recruited", 0)
        timer.Simple(3,function()
            if not IsValid(recruiter) then return end
            recruiter:ConCommand("recruiter_bar");
            recruiter:ConCommand("recruiter_join");
        end)

    else
        local recruited = recruiter:GetPData("recruiter.recruited", 0)
        recruiter:SetNWInt("recruiter.recruited", tonumber(recruited))

        local goal = recruiter:GetPData("recruiter.goal", 0)
        recruiter:SetNWInt("recruiter.goal", tonumber(goal))
    end
    recruiters.setRecruiterRewards( recruiter )
end


recruiters.setRecruiterRewards = function( recruiter )
    local recruited = tonumber(recruiter:GetNWInt("recruiter.recruited",0) )
    local oldGoal = tonumber(recruiter:GetNWInt("recruiter.goal",0))
    local goal = 10
    local reward = false
    local rewarded = 5000

    -- Get the reward

    for k,v in SortedPairs(recruiters.rewards) do

        local rewardnr = tonumber(v)
        k = tonumber(k)

        if recruited < tonumber(k)  and (not reward ) then
            reward = rewardnr
            goal = k
        elseif recruited > k and rewardnr then
            rewarded = rewarded +  tonumber(rewardnr)
        end
    end

    if oldGoal < goal then

        local gotpoints = recruiters.rewards[oldGoal]
        if gotpoints and tonumber(recruiter:GetPData("recruiters.gotlastaward"..goal,0) or 0) + 1500 < os.time() then
             recruiter:SetNWString("recruiter.alertText", "Reached "..oldGoal.." joiners!!! "..gotpoints.." rewarded!")
             timer.Create("recruiter.alertText",8,1,function() if IsValid(recruiter) then recruiter:SetNWString("recruiter.alertText","") end end)
             recruiter:SetPData("recruiters.gotlastaward"..goal, os.time())
             if server_id != 999 and recruiter.PS_GivePoints and recruiter.PS_Points then
                 recruiter:PS_GivePoints(tonumber(gotpoints))
                 recruiter:PS_Notify( "You earned "..gotpoints.." points!" )
             end
        end
    end


    rewarded = rewarded + (recruited * recruiters.singlereward)

    local text = "Players recruited: "..recruited.." of "..goal.." | Reward: "..reward.." points | Earned this session: "..rewarded.."!"

    local pointsText = "Rewards: "..recruiters.singlereward.." per player";
    local numberoftext = 0
    for k,v in SortedPairs(recruiters.rewards) do
        if recruited < k and numberoftext < 3 then
            pointsText = pointsText.." | "..recruiters.rewards[k].." "..k.." players"
            numberoftext = numberoftext +1
        end
    end

    local alertText = 0

    recruiter:SetNWInt("recruiter.goal", goal)
    recruiter:SetNWInt("recruiter.reward", reward)
    recruiter:SetNWInt("recruiter.rewarded", rewarded)
    recruiter:SetNWString("recruiter.toptext", text)
    recruiter:SetNWString("recruiter.lowertext", pointsText)

    recruiter:SetPData("recruiter.goal", goal)
    recruiter:SetPData("recruiter.recruited", recruited)

end

recruiters.checkExistingPlayers = function()
    local recruiter = recruiters.getRecruiter()
    if recruiter then
        if recruiters.debugEnabled then
            print("===REQ== Checking existing players")
        end
        for k,ply in pairs(player.GetHumans()) do
            local gotplayer = tonumber(recruiter:GetPData("recruited_"..ply:SteamID(),0) or 0)
            if not gotplayer then gotplayer = 0 end
            if (recruiter ~= ply and (gotplayer + 43200) < os.time()) then
                local recruited =  tonumber(recruiter:GetPData("recruiter.recruited", 0)) + 1

                -- Set the recruiters count data
                recruiter:SetPData("recruited_"..ply:SteamID(), os.time())
                recruiter:SetPData("recruiter.recruited", recruited)
                recruiter:SetNWInt("recruiter.recruited", recruited )

                -- Reward the recruiter
                recruiters.rewardRecruiter( recruiter )
                recruiters.rewardJoiner( ply )
                recruiters.setRecruiterRewards( recruiter )

                -- Sent it to the website
                local sendTable = {
                    ['key']     = gb_json.settings.key,
                    ['action']  = "player_addrecruitpoint",
                    ['steam']   = tostring(recruiter:SteamID()),
                }
                http.Post( gb_json.settings.url, sendTable, function(data)
                    print("==REQ== Saved recruiterdata "..data)
                end);
            end
        end
    end
end

recruiters.playerJoin = function(ply)
    if not IsValid(ply) or not ply:IsPlayer() then return end
    local recruiter = recruiters.getRecruiter()

    recruiters.checkExistingPlayers()

    if recruiter and recruiter:SteamID() == ply:SteamID() then
        if recruiters.debugEnabled then
            print("===REQ== Player is RECRUITER ON JOIN "..recruiter:Nick())
        end
        recruiter:SetNWInt("recruiter.goal",tonumber(recruiter:GetPData("recruiter.goal",0)))
        recruiter:SetNWInt("recruiter.recruited",tonumber(recruiter:GetPData("recruiter.recruited",0) ))

        recruiters.setRecruiter( ply )
        timer.Simple(4,function()
            if not IsValid(recruiter) then return end
            recruiter:ConCommand("recruiter_bar");
            recruiter:ConCommand("recruiter_join");
        end)
    end

    if not recruiter and recruiters.isServerEmpty()  then
        if recruiters.debugEnabled then
            print("===REQ== SETTING RECRUITER, SERVER EMPTY AND NOT RECRUITER")
        end
        recruiters.setRecruiter( ply, true )
    end
end
hook.Add("PlayerInitialSpawn","recruiters.playerJoin",recruiters.playerJoin);



recruiters.checkPlayers = function()

    local timeout = 60
    if server_id == 999 then timeout = 10 end
    timer.Create("recruiters.checkPlayers", timeout, 0, function()
        if recruiters.debugEnabled then
            print("===REQ== Updating recruiter 120secs")
        end
        recruiters.resetRecruiter()
        if not recruiters.isServerEmpty() then
            recruiters.checkExistingPlayers()
        end

        local players = table.Count(player.GetHumans())
        if players > 0 then
            if recruiters.debugEnabled then
                print("===REQ== Server not empty, setting time")
            end
            recruiters.setData("lasttime",os.time())
        end

    end)
end
hook.Add( "Initialize", "recruiters.checkPlayers", recruiters.checkPlayers)