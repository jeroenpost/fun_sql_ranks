net.Receive("ModReqLogs", function( len, pl )

	local logs = net.ReadTable()
	local selected
	local page = 1
	local pages = 0
	for i = 1, #logs, 13 do
		pages = pages + 1
	end
	
	if(pages == 0) then page = 0 end
	local lessthan = 13 * page
	local greaterthan = 13 * (page - 1)
	
	
	local MainFrame = vgui.Create("DPanel")
	MainFrame:SetSize( 700, 450 )
	MainFrame:Center() 
	MainFrame:SetAlpha( 0 )
	MainFrame:AlphaTo( 255, .25, 0, function(animData, pnl)
	end)
	MainFrame.Paint = function()
		draw.RoundedBox( 8, 0, 0, MainFrame:GetWide(), MainFrame:GetTall(), Color( 0, 0, 0, 210 ) )
		surface.SetTextColor( 255, 255, 255, 255 )
		surface.SetTextPos( MainFrame:GetWide()/2-72, 0 ) 
		surface.SetFont("Trebuchet24")
		surface.DrawText( "   ModReq Logs" )

		draw.RoundedBoxEx(8, 0, 0, MainFrame:GetWide(), 25, Color(100, 100, 100, 225), true, true, false, false)
	end		
	MainFrame:MakePopup()
	
	
	local TicketID = vgui.Create( "DButton" )
	TicketID:SetText( "Player Name" )
	TicketID:SetPos( 50, 48 )
	TicketID:SetSize( 100, 15 )
	TicketID.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, TicketID:GetWide(), TicketID:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	TicketID:SetParent( MainFrame )
	
	local Messages = vgui.Create( "DButton" )
	Messages:SetText( "Log Entry" )
	Messages:SetPos( 252, 48 )
	Messages:SetSize( 398, 15 )
	Messages.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, Messages:GetWide(), Messages:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	Messages:SetParent( MainFrame )

	local PlyName = vgui.Create( "DButton" )
	PlyName:SetText( "Staff" )
	PlyName:SetPos( 151, 48 )
	PlyName:SetSize( 100, 15 )
	PlyName.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, PlyName:GetWide(), PlyName:GetTall(), Color(255, 255, 255, 255), true, true, false, false)
	end
	PlyName:SetParent( MainFrame )
	
	local RightArrow = vgui.Create( "DButton" )
	RightArrow:SetImage("materials/icon16/arrow_right.png")	
	RightArrow:SetText("")
	RightArrow:SetPos( 388, 398 )
	RightArrow:SetSize( 25, 20 )
	RightArrow:SetParent( MainFrame )
	if(pages == 1) then RightArrow:SetDisabled(true) end
	
	local LeftArrow = vgui.Create( "DButton" )
	LeftArrow:SetImage("materials/icon16/arrow_left.png")	
	LeftArrow:SetText("")
	LeftArrow:SetPos( 278, 398 )
	LeftArrow:SetSize( 25, 20 )
	LeftArrow:SetDisabled(true)
	LeftArrow:SetParent( MainFrame )
	
	local CloseButton = vgui.Create( "DButton" )
	CloseButton:SetText( "X" )	
	CloseButton:SetPos( 675, 0 )
	CloseButton:SetSize( 25, 25 )
	CloseButton:SetParent( MainFrame )
	CloseButton.Paint = function() 
		draw.RoundedBoxEx(8, 0, 0, CloseButton:GetWide(), CloseButton:GetTall(), Color(50, 50, 50, 225), false, true, false, false)
	end
	CloseButton.DoClick = function ()
		MainFrame:AlphaTo( 0, .25, 0, function(animData, pnl)
			MainFrame:Remove()
		end)
	end
	
	local backgroundPanel = vgui.Create( "DPanel", MainFrame )
	backgroundPanel:SetSize( 600, 340 )
	backgroundPanel:SetPos(50, 65)
	backgroundPanel.Paint = function()
	draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
		surface.SetDrawColor( 0, 0, 0, 255 )
		surface.DrawLine( 100, 0, 100, 321)
		surface.DrawLine( 201, 0, 201, 321)
	
		for k, v in pairs(logs) do
			if(k > greaterthan) and (k <= lessthan) then
				surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
			end
		end
	end		
		
	local NumPages = vgui.Create("DLabel", MainFrame)
	NumPages:SetPos( ( MainFrame:GetWide() / 2 ) - 38 , 400 )
	NumPages:SetFont("Trebuchet18")
	NumPages:SetTextColor(Color(255,255,255,255))
	NumPages:SetText( "Page: " .. page .. " of " .. pages )
	NumPages:SizeToContents()
	local log = {}
	for k, v in pairs(logs) do
		if(k > greaterthan) and (k <= lessthan) then
			table.insert(log, v)
		end
	end
	for i=1, 13 do
		if(log[i]) then
			
			steamworks.RequestPlayerInfo( log[i].reportee )
			steamworks.RequestPlayerInfo( log[i].claiment )
			
			local test2 = vgui.Create( "DLabel", backgroundPanel )
			test2:SetPos( 210, -1 + (25 * (i - 1) ) )
			test2:SetTextColor(color_black)
			test2:SetText(log[i].message)
			test2:SetSize( 425, 25 )
			
			
			local test = vgui.Create( "DLabel" )
			test:SetPos( 104, 3 + (25 * (i - 1) ))
			test:SetSize(90, 20)
			test:SetTextColor(color_black)
			test:SetText("Loading")
			timer.Simple(1, function()
				if(test:IsValid()) then
					test:SetText( steamworks.GetPlayerName( log[i].claiment ) )
				end
			end)				
			test:SetParent(backgroundPanel)
			
			local Mark = vgui.Create( "DLabel" )
			Mark:SetPos(4, 3 + (25 * (i - 1) ))
			Mark:SetSize(90, 20)
			Mark:SetTextColor(color_black)
			Mark:SetText("Loading")
			timer.Simple(1, function()
				if(Mark:IsValid()) then
					Mark:SetText( steamworks.GetPlayerName( log[i].reportee ) )
				end
			end)
			Mark:SetParent(backgroundPanel)
		end
	end
	
	RightArrow.DoClick = function ()
		page = page + 1
		if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
		if page >= pages then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
		NumPages:SetText("Page: " .. page .. " of " .. pages)
		local lessthan = 13 * page
		local greaterthan = 13 * (page - 1)
		local backgroundPanel = vgui.Create( "DPanel", MainFrame )
		backgroundPanel:SetSize( 600, 340 )
		backgroundPanel:SetPos( 50, 65 )
		backgroundPanel:SetAlpha( 0 )
		backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
		end)
		backgroundPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
			surface.SetDrawColor( 0, 0, 0, 255 )
			surface.DrawLine( 100, 0, 100, 321)
			surface.DrawLine( 201, 0, 201, 321)
			
			for k, v in pairs(logs) do
				if(k > greaterthan) and (k <= lessthan) then
					surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
				end
			end
		end		
		
		local log = {}
		for k, v in pairs(logs) do
			if(k > greaterthan) and (k <= lessthan) then
				table.insert(log, v)
			end
		end
		
		for i=1, 13 do
			if(log[i]) then
			
				steamworks.RequestPlayerInfo( log[i].reportee )
				steamworks.RequestPlayerInfo( log[i].claiment )
				
				local test2 = vgui.Create( "DLabel", backgroundPanel )
				test2:SetPos( 210, -1 + (25 * (i - 1) ) )
				test2:SetTextColor(color_black)
				test2:SetText(log[i].message)
				test2:SetSize( 425, 25 )
				
				
				local test = vgui.Create( "DLabel" )
				test:SetPos( 104, 3 + (25 * (i - 1) ))
				test:SetSize(90, 20)
				test:SetTextColor(color_black)
				test:SetText("Loading")
				timer.Simple(1, function()
					if(test:IsValid()) then
						test:SetText( steamworks.GetPlayerName( log[i].claiment ) )
					end
				end)				
				test:SetParent(backgroundPanel)
				
				local Mark = vgui.Create( "DLabel" )
				Mark:SetPos(4, 3 + (25 * (i - 1) ))
				Mark:SetSize(90, 20)
				Mark:SetTextColor(color_black)
				Mark:SetText("Loading")
				timer.Simple(1, function()
					if(Mark:IsValid()) then
						Mark:SetText( steamworks.GetPlayerName( log[i].reportee ) )
					end
				end)
				Mark:SetParent(backgroundPanel)
			end
		end
	end 
	
	LeftArrow.DoClick = function ()
		page = page - 1
		NumPages:SetText("Page: " .. page .. " of " .. pages)
		if(page > 1) then LeftArrow:SetDisabled(false) else LeftArrow:SetDisabled(true) end
		if page >= pages then RightArrow:SetDisabled(true) else RightArrow:SetDisabled(false) end
		local lessthan = 13 * page
		local greaterthan = 13 * (page - 1)
		local backgroundPanel = vgui.Create( "DPanel", MainFrame )
		backgroundPanel:SetSize( 600, 340 )
		backgroundPanel:SetPos(50, 65)
		backgroundPanel:SetAlpha( 0 )
		backgroundPanel:AlphaTo( 255, 1.25, 0, function(animData, pnl)
		end)
		backgroundPanel.Paint = function()
		draw.RoundedBoxEx(8, 0, 0, backgroundPanel:GetWide() + 50, backgroundPanel:GetTall() - 20, Color(255, 255, 255, 255))
			surface.SetDrawColor( 0, 0, 0, 255 )
			surface.DrawLine( 100, 0, 100, 321)
			surface.DrawLine( 201, 0, 201, 321)
		
			for k, v in pairs(logs) do
				if(k > greaterthan) and (k <= lessthan) then
					surface.DrawLine( 0 , 21 + (25 * ((k - greaterthan) - 1)), 600, 21 + (25 * ((k - greaterthan) - 1)) )
				end
			end
		end		
		
		local log = {}
		for k, v in pairs(logs) do
			if(k > greaterthan) and (k <= lessthan) then
				table.insert(log, v)
			end
		end
		
		for i=1, 13 do
			if(log[i]) then
				
				steamworks.RequestPlayerInfo( log[i].reportee )
				steamworks.RequestPlayerInfo( log[i].claiment )
				
				local test2 = vgui.Create( "DLabel", backgroundPanel )
				test2:SetPos( 210, -1 + (25 * (i - 1) ) )
				test2:SetTextColor(color_black)
				test2:SetText(log[i].message)
				test2:SetSize( 425, 25 )
				
				
				local test = vgui.Create( "DLabel" )
				test:SetPos( 104, 3 + (25 * (i - 1) ))
				test:SetSize(90, 20)
				test:SetTextColor(color_black)
				test:SetText("Loading")
				timer.Simple(1, function()
					if(test:IsValid()) then
						test:SetText( steamworks.GetPlayerName( log[i].claiment ) )
					end
				end)				
				test:SetParent(backgroundPanel)
				
				local Mark = vgui.Create( "DLabel" )
				Mark:SetPos(4, 3 + (25 * (i - 1) ))
				Mark:SetSize(90, 20)
				Mark:SetTextColor(color_black)
				Mark:SetText("Loading")
				timer.Simple(1, function()
					if(Mark:IsValid()) then
						Mark:SetText( steamworks.GetPlayerName( log[i].reportee ) )
					end
				end)
				Mark:SetParent(backgroundPanel)
			end
		end
	end
	
end)	