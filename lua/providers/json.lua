
gb_json = {}
gb_json.settings = {}
gb_json.settings.url = "https://ttt-fun.com/ttt_fun_api"
gb_json.settings.key = "02938470usadfkjhbasdf9934"

gb_json.online = true
gb_json.lastcheck = 0
gb_json.debug = false

gb_json.statuscheck = function()
    http.Post( gb_json.settings.url ,{['key'] = gb_json.settings.key ,['action']="statuscheck" },
        function( body, len, headers, code )
            if body == "SUCCESS" then
                gb_json.set_online()
            else
                gb_json.set_offline()
            end
        end,
        function()
            gb_json.set_offline()
        end)
end

gb_json.set_offline = function()
    gb_json.online = false
    for k,v in pairs(player.GetAll()) do
        v:ChatPrint( "[WARNING] The database is offline. Your points + time + rank won't be saved/fetched until fixed")
    end
    print("[WARNING] MASTER DB OFFLINE!")
end

gb_json.set_online = function()
    gb_json.online = true
    print("[JSONCHECK] = OK")
    if  gb_json.online == false then
        for k,v in pairs(player.GetAll()) do
            v:ChatPrint( "[YAY] The Database is back online! Your points will be saved")
        end
        print("[YAY] DB = BACK!")
    end
end

hook.Add("Think","gb_json statuscheck", function()
    if gb_json.lastcheck + 60 < CurTime() then
        gb_json.lastcheck = CurTime()
        gb_json.statuscheck()
    end
end)


if not PROVIDER then PROVIDER = {} end
PROVIDER.Fallback = 'pdata'
function PROVIDER:SetData(ply, points, items)
    fun_api.save_user_data ( ply, true )
end


-- encoding
function  gb_json.base64enc(data)
    local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    return ((data:gsub('.', function(x)
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

-- decoding
function  gb_json.base64dec(data)
    local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end


