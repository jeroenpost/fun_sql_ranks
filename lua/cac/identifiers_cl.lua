CAC.Identifiers        = CAC.Identifiers        or {}
CAC.InverseIdentifiers = CAC.InverseIdentifiers or {}

CAC.ID40582e12 =
{
	"fd35edd281c2b469cd42a76ca02a9a89",
	"07ff2df0acb8d9718042b31aa07773e7df87",
	"af3505191a1da801f331ca7d16f5f17badccc6a67af6fa449da034a451ea5e3e55266d4d",
	"efeb9a153c258c43ee60c927f177220c684b143adcc2344fe744",
	"ea9f0a97457e675da157c0752a88cb5e388735920713b1ed9687c4419bb75e83fc3b7446",
	"73826d47e5a408026e12ea54a6aeb6a68042c45770d5d78441e256d2b010d9323bc0ffcf435fb6c68980118746c44c65373285af93d070f0fb905d71a37cb5f1",
}

CAC.Identifiers.AdminChannelName = "⁬‭⁬⁭⁪​‎⁪‬.‌​⁭⁮‌⁭​⁬⁭"

for k, v in pairs (CAC.Identifiers) do
	CAC.InverseIdentifiers [v] = k
end
